# Example 016: Optimized scene
<br><b>Changelog:</b>
1. Merge `Lamp.java` with `Lamps.java` and `Lamp.ShadowMapping`
2. Other optimizations and fixes
