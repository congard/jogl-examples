package example.congard.jogl.example016.optimizedscene;

import java.nio.IntBuffer;

import com.jogamp.opengl.GL;
import com.jogamp.opengl.GL2;
import com.jogamp.opengl.GL3;

import lib.congard.ml.geometry.Geometry;
import lib.congard.ml.graphics.GMatrix;

/**
 * 
 * @author congard
 *
 */
public class Lamp {
	public static final float FAR_PLANE = 32;
	// real coordinates (in world)
	public float[] pos = new float[3];
	// light color
	public float[] color = new float[3];
	// ambient strength param; recommended: 0.01
	public float ambientStrength;
	// diffuse strength param; recommended: 1
	public float diffuseStrength;
	// specular strength param; recommended: 0.1
	public float specularStrength;
	// constant term; usually kept at 1.0
	public float kc;
	// linear term
	public float kl;
	// quadratic term
	public float kq;
	// shininess; recommended: 32
	public int shininess;

	public Lamp initShadowMapping(GL3 gl) {
		createDepthMap(gl);
		createDepthMapFBO(gl);
		updateMatrices();
		init(gl);
		return this;
	}
	
	// This n value is the shininess value of the highlight.
	// The higher the shininess value of an object, 
	// the more it properly reflects the light instead of scattering it all around and
	// thus the smaller the highlight becomes
	
	/* --- to access shaders --- */
	public static int lightPosLocation;
	public int[] propLocations = new int[9];
	
	public void loadLocations(GL2 gl, int programId, int id) {
		propLocations[0] = gl.glGetUniformLocation(programId, "u_Lamps[" + id + "].ambientStrength");
		propLocations[1] = gl.glGetUniformLocation(programId, "u_Lamps[" + id + "].diffuseStrength");
		propLocations[2] = gl.glGetUniformLocation(programId, "u_Lamps[" + id + "].specularStrength");
		propLocations[3] = gl.glGetUniformLocation(programId, "u_Lamps[" + id + "].shininess");
		propLocations[4] = gl.glGetUniformLocation(programId, "u_Lamps[" + id + "].lampPos");
		propLocations[5] = gl.glGetUniformLocation(programId, "u_Lamps[" + id + "].lampColor");
		propLocations[6] = gl.glGetUniformLocation(programId, "u_Lamps[" + id + "].kc");
		propLocations[7] = gl.glGetUniformLocation(programId, "u_Lamps[" + id + "].kl");
		propLocations[8] = gl.glGetUniformLocation(programId, "u_Lamps[" + id + "].kq");
	}
	
	public void push_ambientStrength(GL2 gl) { gl.glUniform1f(propLocations[0], ambientStrength); }

	public void push_diffuseStrength(GL2 gl) { gl.glUniform1f(propLocations[1], diffuseStrength); }

	public void push_specularStrength(GL2 gl) { gl.glUniform1f(propLocations[2], specularStrength); }

	public void push_shininess(GL2 gl) { gl.glUniform1i(propLocations[3], shininess); }

	public void push_lampPos(GL2 gl) { gl.glUniform3f(propLocations[4], pos[0], pos[1], pos[2]); }

	public void push_lampColor(GL2 gl) { gl.glUniform3f(propLocations[5], color[0], color[1], color[2]); }

	public void push_kc(GL2 gl) { gl.glUniform1f(propLocations[6], kc); }

	public void push_kl(GL2 gl) { gl.glUniform1f(propLocations[7], kl); }

	public void push_kq(GL2 gl) { gl.glUniform1f(propLocations[8], kq); }

	public void pushAll(GL2 gl) {
        push_ambientStrength(gl);
        push_diffuseStrength(gl);
        push_specularStrength(gl);
        push_shininess(gl);
        push_lampPos(gl);
        push_lampColor(gl);
        push_kc(gl);
        push_kl(gl);
        push_kq(gl);
    }
	
	/* --- shadow mapping --- */
	public IntBuffer depthMapFBO, depthCubemap;
	public int resolution;
	public float[] lightProjection = new float[16];
	public float[][] lightViews = new float[6][16];
	public float[][] lightSpaceMatrices = new float[6][16];
	public int[] shadowMatricesLocations = new int[6];
	
	public void initShadowMatrices(GL2 gl, int programId, String array) {
		for (int i = 0; i < 6; i++) shadowMatricesLocations[i] = gl.glGetUniformLocation(programId, array + "[" + i + "]");
	}
	
	public void setLightPos(GL2 gl) {
		gl.glUniform3f(lightPosLocation, pos[0], pos[1], pos[2]);
	}
	
	public void setShadowMatrices(GL2 gl) {
		for (int i = 0; i < 6; i++) gl.glUniformMatrix4fv(shadowMatricesLocations[i], 1, false, lightSpaceMatrices[i], 0);
	}
	
	public void updateMatrices() {
		GMatrix.perspectiveM(lightProjection, Geometry.toRadiansf(90), 1, 1, FAR_PLANE);
		GMatrix.setLookAtM(lightViews[0], 0, pos[0], pos[1], pos[2], pos[0] + 1, pos[1], pos[2], 0, -1, 0);
		GMatrix.setLookAtM(lightViews[1], 0, pos[0], pos[1], pos[2], pos[0] - 1, pos[1], pos[2], 0, -1, 0);
		GMatrix.setLookAtM(lightViews[2], 0, pos[0], pos[1], pos[2], pos[0], pos[1] + 1, pos[2], 0, 0, 1);
		GMatrix.setLookAtM(lightViews[3], 0, pos[0], pos[1], pos[2], pos[0], pos[1] - 1, pos[2], 0, 0, -1);
		GMatrix.setLookAtM(lightViews[4], 0, pos[0], pos[1], pos[2], pos[0], pos[1], pos[2] + 1, 0, -1, 0);
		GMatrix.setLookAtM(lightViews[5], 0, pos[0], pos[1], pos[2], pos[0], pos[1], pos[2] - 1, 0, -1, 0);
		for (int i = 0; i < 6; i++) GMatrix.multiplyMM(lightSpaceMatrices[i], lightProjection, lightViews[i]);
	}
	
	public void createDepthMapFBO(GL3 gl) {
		depthMapFBO = IntBuffer.allocate(1);
		gl.glGenFramebuffers(1, depthMapFBO);
	}
	
	public void createDepthMap(GL3 gl) {
		depthCubemap = IntBuffer.allocate(1);
		gl.glGenTextures(1, depthCubemap);
	}
	
	public void init(GL3 gl) {
		gl.glBindTexture(GL.GL_TEXTURE_CUBE_MAP, depthCubemap.get(0));
		for (int i = 0; i < 6; i++) gl.glTexImage2D(GL.GL_TEXTURE_CUBE_MAP_POSITIVE_X + i, 0, GL2.GL_DEPTH_COMPONENT, resolution, resolution, 0, GL2.GL_DEPTH_COMPONENT, GL.GL_FLOAT, null);
		gl.glTexParameteri(GL.GL_TEXTURE_CUBE_MAP, GL.GL_TEXTURE_MAG_FILTER, GL.GL_NEAREST);
		gl.glTexParameteri(GL.GL_TEXTURE_CUBE_MAP, GL.GL_TEXTURE_MIN_FILTER, GL.GL_NEAREST);
		gl.glTexParameteri(GL.GL_TEXTURE_CUBE_MAP, GL.GL_TEXTURE_WRAP_S, GL.GL_CLAMP_TO_EDGE);
		gl.glTexParameteri(GL.GL_TEXTURE_CUBE_MAP, GL.GL_TEXTURE_WRAP_T, GL.GL_CLAMP_TO_EDGE);
		gl.glTexParameteri(GL.GL_TEXTURE_CUBE_MAP, GL2.GL_TEXTURE_WRAP_R, GL.GL_CLAMP_TO_EDGE);
		gl.glBindFramebuffer(GL.GL_FRAMEBUFFER, depthMapFBO.get(0));
		gl.glFramebufferTexture(GL.GL_FRAMEBUFFER, GL.GL_DEPTH_ATTACHMENT, depthCubemap.get(0), 0);
		gl.glDrawBuffer(GL.GL_NONE);
		gl.glReadBuffer(GL.GL_NONE);
		gl.glBindFramebuffer(GL.GL_FRAMEBUFFER, 0);
		gl.glBindTexture(GL.GL_TEXTURE_CUBE_MAP, 0);
	}
	
	public void begin(GL2 gl) {
		gl.glBindFramebuffer(GL.GL_FRAMEBUFFER, depthMapFBO.get(0));
		gl.glActiveTexture(GL.GL_TEXTURE0);
		gl.glBindTexture(GL.GL_TEXTURE_CUBE_MAP, depthCubemap.get(0));
		gl.glViewport(0, 0, resolution, resolution);
	}
	
	public void end(GL2 gl) {
		gl.glBindFramebuffer(GL.GL_FRAMEBUFFER, 0);
		gl.glBindTexture(GL.GL_TEXTURE_CUBE_MAP, 0);
	}
}
