# JOGL Examples
JOGL (Java OpenGL) Examples
<br><small>Previous repo: [github](https://github.com/congard/jogl-examples)
<br>My previous profile on [github](https://github.com/congard/)</small>

1. [Shaders with 2 matrices](https://gitlab.com/congard/jogl-examples/tree/master/001-shaders-2matrices)
1. Shaders with 3 matrices
1. [Shaders with 3 matrices (Model-View-Projection) and simple lighting](https://gitlab.com/congard/jogl-examples/tree/master/003-3matrices_mvp-simpe-lighting)
1. [Shaders with 3 matrices (Model-View-Projection) and simple lighting with several lamps](https://gitlab.com/congard/jogl-examples/tree/master/004-several-lamps)
1. [Shaders with 3 matrices (Model-View-Projection), simple lighting with several lamps and texture](https://gitlab.com/congard/jogl-examples/tree/master/005-lighting-and-texture)
1. [Ambient+diffuse+specular colorful lighting with several lamps, animation and texture](https://gitlab.com/congard/jogl-examples/tree/master/006-colorful-ads-lighting)
1. [Ambient+diffuse+specular colorful lighting with several lamps, animation, texture and lamps prop](https://gitlab.com/congard/jogl-examples/tree/master/007-lamps-properties)
    <br><br>Small fix for lamps:
    <br>`// updating lamps eye space position`
		<br>`for (int i = 0; i<lamps.length; i++) lamps[i].calculateLampPosInEyeSpace(mViewMatrix);`
    <br>Add this to `display` method
    <br>This is necessary to add in the event that on your scene the lamps do not move, but the camera moves
    <br><br>
1. [Ambient+diffuse+specular colorful lighting with several lamps, animation, texture, lamps prop, attenuation and OBJ models loading](https://gitlab.com/congard/jogl-examples/tree/master/008-attenuation-objmodels)
1. [Ambient+diffuse+specular colorful lighting with several lamps, animation, texture, lamps prop, attenuation and, models loading and lighting maps](https://gitlab.com/congard/jogl-examples/tree/master/009-lightingmaps-ads)
1. [+ Normal Mapping](https://gitlab.com/congard/jogl-examples/tree/master/010-normalmapping)<br>[Video](https://www.youtube.com/watch?v=l_JgyE9BKo0)
1. [+ Shadow Mapping with soft shadows](https://gitlab.com/congard/jogl-examples/tree/master/011-shadowmapping)
1. [+ Bloom and gamma configuration](https://gitlab.com/congard/jogl-examples/tree/master/012-bloom)
    <br><small>Some fixes added</small>
1. [+ Fast Shadow Mapping](https://gitlab.com/congard/jogl-examples/tree/master/013-fast-shadow-mapping)
1. [+ Point lighting](https://gitlab.com/congard/jogl-examples/tree/master/014-point-lighting)
    <br>In the previous examples, there is an error related to lighting. To fix it, just replace `vec3 viewDir = normalize(u_ViewPos - fragPos)` with `vec3 viewDir = normalize(mat3(u_ViewMatrix) * u_ViewPos - fragPos)` in fragment shader
1. [+ CMF models + animated scene + light source object](015-cmf-animated-scene)
1. [Optimized scene (based on example 015)](016-optimized-scene)
