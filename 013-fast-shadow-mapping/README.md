# 013: Fast Shadow Mapping
1. Before that, all the shadow maps were packed into one buffer, and sent to `sampler2DArray`. It was slow enough, and for the array to work, you needed an extension that not all graphics devices support. Now, just using simple array `sampler2D shadowMaps[MAX_LAMPS_COUNT]`, which gives acceleration in dozens of times, and at a high resolution of the map, even in hundreds.
1. Updated libcm to 1.1 version
1. Generating the kernel for blur
