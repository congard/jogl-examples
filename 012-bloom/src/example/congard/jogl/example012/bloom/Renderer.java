package example.congard.jogl.example012.bloom;

import java.io.File;
import java.nio.ByteBuffer;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;

import com.jogamp.common.nio.Buffers;
import com.jogamp.opengl.GL;
import com.jogamp.opengl.GL2;
import com.jogamp.opengl.GLAutoDrawable;
import com.jogamp.opengl.GLEventListener;

import example.congard.jogl.example012.bloom.ShaderProgram.GLSLValue;
import example.congard.jogl.example012.bloom.TextureUtils.Mapping;
import example.congard.jogl.example012.bloom.TextureUtils.TextureArray;
import free.lib.congard.ml.geometry.Geometry;
import free.lib.congard.ml.graphics.GMatrix;
import free.lib.congard.objloader.LoaderConstants;
import free.lib.congard.objloader.Model;

/**
 * Rendering with bloom
 * But only for triangulated models
 * 
 * @author Congard
 * 
 * Links:
 * I:
 * t.me/congard
 * gitlab.com/congard
 * dbcongard@gmail.com
 * 
 * serhiy:
 * github.com/serhiy
 */
public class Renderer implements GLEventListener {
	private Lamp[] lamps;
	private ShaderProgram shaderProgram, shaderShadowProgram;
	private float[] mProjectionMatrix = new float[16];
	private float[] mViewMatrix = new float[16];
	private float[] mModelMatrix = new float[16];
	private float[] mMVPMatrix = new float[16];
	private float[] mMVMatrix = new float[16];
	private float[][] rotationAnglesRad;
	private float eyeX = 0, eyeY = 8, eyeZ = 12, // положение камеры / camera position
			lookX = 0, lookY = 4.5f, lookZ = 0,  // точка направления т.е куда смотрим / look point
			upX = 0, upY = 1, upZ = 0; // смотрим по оси у / look at the axis y
	private Lamps structLamps;
	private ModelObject cube, floor, rapier, lucy;
	private Mapping mapping;
	private TextureArray shadowMaps;
	private ModelObject models[] = new ModelObject[4];
	private AdvancedRendering ar = new AdvancedRendering();
	//private Debug debug;
	
	private final static int LAMPS_COUNT = 1; // 1 lamp on scene
	private final static int SHADOW_MAP_WIDTH = 1024, SHADOW_MAP_HEIGHT = 1024;
	
	// window size
	private int WIN_W = 1024, WIN_H = 1024;
	
	@Override
	public void init(GLAutoDrawable glAutoDrawable) {
		GL2 gl2 = glAutoDrawable.getGL().getGL2();

		File vertexShader = new File("./resources/shaders/vertex_shader.glsl");
		File fragmentShader = new File("./resources/shaders/fragment_shader.glsl");
		File vertexShadowShader = new File("./resources/shaders/vertex_shadow_shader.glsl");
		File fragmentShadowShader = new File("./resources/shaders/fragment_shadow_shader.glsl");
		
		shaderProgram = new ShaderProgram();
		if (!shaderProgram.init(gl2, vertexShader, fragmentShader)) {
			throw new IllegalStateException("Unable to initiate the shaders!");
		}
		
		shaderShadowProgram = new ShaderProgram();
		if (!shaderShadowProgram.init(gl2, vertexShadowShader, fragmentShadowShader)) {
			throw new IllegalStateException("Unable to initiate the shaders!");
		}
		
		shaderProgram.loadValuesIds(gl2, 
				new GLSLValue("a_Position", GLSLValue.ATTRIB),
				new GLSLValue("a_Normal", GLSLValue.ATTRIB),
				new GLSLValue("a_Texture", GLSLValue.ATTRIB),
				new GLSLValue("a_Tangent", GLSLValue.ATTRIB),
				new GLSLValue("a_Bitangent", GLSLValue.ATTRIB),
				new GLSLValue("u_MVPMatrix", GLSLValue.UNIFORM),
				new GLSLValue("u_MVMatrix", GLSLValue.UNIFORM),
				new GLSLValue("u_LampsCount", GLSLValue.UNIFORM),
				new GLSLValue("u_ViewPos", GLSLValue.UNIFORM),
				new GLSLValue("shadowMaps", GLSLValue.UNIFORM)
		);
		
		shaderShadowProgram.loadValuesIds(gl2, 
				new GLSLValue("a_Position", GLSLValue.ATTRIB),
				new GLSLValue("u_LightSpaceMatrix", GLSLValue.UNIFORM),
				new GLSLValue("u_ModelMatrix", GLSLValue.UNIFORM)
		);
		
		//debug = new Debug(gl2);
		
		System.out.println("Shaders created");
		
		shadowMaps = new TextureArray();
		shadowMaps.layerCount = LAMPS_COUNT;
		shadowMaps.width = SHADOW_MAP_WIDTH;
		shadowMaps.height = SHADOW_MAP_HEIGHT;
		shadowMaps.createTexture(gl2);
		
		mapping = new Mapping(gl2, shaderProgram.getProgramId());
		mapping.loadLocations();
		
		System.out.println("Loading models");
		// loading obj models >>
		cube = new ModelObject(loadModel(new File("./resources/models/cube/cube_triangulated.obj")), 
				TextureUtils.loadTexture(gl2, new File("./resources/textures/cube/cube_ambient.jpg")),
				TextureUtils.loadTexture(gl2, new File("./resources/textures/cube/cube_diffuse.jpg")),
				TextureUtils.loadTexture(gl2, new File("./resources/textures/cube/cube_specular.jpg")),
				TextureUtils.loadTexture(gl2, new File("./resources/textures/cube/cube_normal.jpg")));
		//lamp = new ModelObject(loadModel(new File("./resources/models/lamp.obj")), TextureUtils.loadTexture(gl2, new File("./resources/textures/lamp.jpg")));
		floor = new ModelObject(loadModel(new File("./resources/models/brickwall/brickwall_triangulated.obj")), 
				TextureUtils.loadTexture(gl2, new File("./resources/textures/brickwall/brickwall.jpg")),
				TextureUtils.loadTexture(gl2, new File("./resources/textures/brickwall/brickwall_normal.jpg")));
		rapier = new ModelObject(loadModel(new File("./resources/models/italian_rapier/italian_rapier_triangulated.obj")));
		rapier.ambientTexture = TextureUtils.loadTexture(gl2, new File("./resources/textures/italian_rapier/diffuse.png"));
		rapier.diffuseTexture = rapier.ambientTexture;
		rapier.specularTexture = TextureUtils.loadTexture(gl2, new File("./resources/textures/italian_rapier/specular.png"));
		rapier.normalTexture = TextureUtils.loadTexture(gl2, new File("./resources/textures/italian_rapier/normal.png"));
		lucy = new ModelObject(loadModel(new File("./resources/models/LucyAngel/Stanfords_Lucy_Angel_triangulated.obj")));
		lucy.ambientTexture = TextureUtils.loadTexture(gl2, new File("./resources/textures/LucyAngel/Stanfords_Lucy_Angel_diffuse.jpg"));
		lucy.diffuseTexture = lucy.ambientTexture;
		lucy.specularTexture = TextureUtils.loadTexture(gl2, new File("./resources/textures/LucyAngel/Stanfords_Lucy_Angel_specular.jpg"));;
		lucy.normalTexture = TextureUtils.loadTexture(gl2, new File("./resources/textures/LucyAngel/Stanfords_Lucy_Angel_normal.jpg"));
		//lamp = new ModelObject(loadModel(new File("./resources/models/lamp.obj")), TextureUtils.loadTexture(gl2, new File("./resources/textures/lamp.jpg")));
		
		floor.angles[0] = -90;
		cube.angles[0] = 0;
		rapier.angles[0] = 90;
		rapier.origin[2] = 3;
		rapier.origin[1] = 3;
		lucy.origin[1] = cube.model.maxY;
		
		// making tangents and bitangents buffers
		// only for TRIANGULATED models
		cube.mkTBBuffers();
		floor.mkTBBuffers();
		rapier.mkTBBuffers();
		lucy.mkTBBuffers();
		// <<
		
		models[0] = cube;
		models[1] = floor;
		models[2] = rapier;
		models[3] = lucy;
		System.out.println("Models loaded");
		
		gl2.glEnable(GL2.GL_DEPTH_TEST);
		gl2.glDepthMask(true);
		gl2.glClearColor(0, 0, 0, 1);
		
		createMatrices();
		
		ar.init(gl2);
		
		structLamps = new Lamps(gl2, shaderProgram, LAMPS_COUNT);
		structLamps.loadLocations();
		lamps = new Lamp[] {
				createLamp(-0, (cube.model.maxY-cube.model.minY)/2 + 5, 5).setColor(1, 1, 1).setAmbientStrength(0.01f).setDiffuseStrength(1).setSpecularStrength(1f).setShininess(32).setAttenuationTerms(1, 0.045f, 0.0075f).initShadowMapping(gl2), // white lamp
				//createLamp(3, (cube.model.maxY-cube.model.minY)/2 + 3, 3).setColor(1, 1, 1).setAmbientStrength(0.01f).setDiffuseStrength(1).setSpecularStrength(1f).setShininess(32).setAttenuationTerms(1, 0.045f, 0.0075f).initShadowMapping(gl2) // white lamp
		};
		// this array need only for animation
		rotationAnglesRad = new float[][] {
			new float[] { (float)Math.sin(Math.toRadians(0.1)), (float)Math.cos(Math.toRadians(0.1)) } // for camera
		};

		// animation thread
		new Thread(new Runnable() {
			float[] cameraPos;
			
			@Override
			public void run() {
				while (true) {
					try {
						Thread.sleep(4);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

					cameraPos = Geometry.rotateAroundYf(0, 0, 0, eyeX, eyeY, eyeZ, rotationAnglesRad[0][0], rotationAnglesRad[0][1]);
					eyeX = cameraPos[0];
					eyeY = cameraPos[1];
					eyeZ = cameraPos[2];
				}
			}
		}).start();
	}
	
	private static Model loadModel(File path) {
		Model m = new Model(path); // path to obj model
		m.enable(LoaderConstants.TEX_VERTEX_2D); // xy texture coords
		m.setDefaultPolyTypes(GL2.GL_TRIANGLES, GL2.GL_QUADS, GL2.GL_POLYGON); // for easy drawing
		m.load(); // loading model
		m.convertToFloatArrays(true, true); // getting texcoords and normals
		m.cleanup();
		return m;
	}
	
	private void createMatrices() {
		System.out.println("Creating matrices");
		createProjectionMatrix(1, 1);
		createViewMatrix();
		createModelMatrix();
		createMVMatrix();
		createMVPMatrix();
	}
	
	private void createProjectionMatrix(int width, int height) {
		float ratio = 1;
        float left = -1;
        float right = 1;
        float bottom = -1;
        float top = 1;
        float near = 2f, far = 96;
        if (width > height) {
            ratio = (float) width / (float) height;
            left *= ratio;
            right *= ratio;
        } else {
            ratio = (float) height / (float) width;
            bottom *= ratio;
            top *= ratio;
        }

        GMatrix.frustumM(mProjectionMatrix, 0, left, right, bottom, top, near, far);
        //System.out.println("Projection matrix created");
	}
	
	private void createViewMatrix() {
		GMatrix.setLookAtM(mViewMatrix, 0, eyeX, eyeY, eyeZ, lookX, lookY, lookZ, upX, upY, upZ);
	}
	
	private void createModelMatrix() {
		GMatrix.setIdentityM(mModelMatrix, 0);
	}
	
	private void createMVMatrix() {
		GMatrix.multiplyMM(mMVMatrix, mViewMatrix, mModelMatrix);
	}
	
	private void createMVPMatrix() {
		GMatrix.multiplyMM(mMVPMatrix, mViewMatrix, mModelMatrix);
		GMatrix.multiplyMM(mMVPMatrix, mProjectionMatrix, mMVPMatrix);
		// Final (Projection * View * Model) matrix
	}
	
	private void bindMatrices(GL2 gl) {
		// bind to matrices
		gl.glUniformMatrix4fv(shaderProgram.getValueId("u_MVPMatrix"), 1, false, mMVPMatrix, 0);
		gl.glUniformMatrix4fv(shaderProgram.getValueId("u_MVMatrix"), 1, false, mMVMatrix, 0);
	}
	
	private Lamp createLamp(float x, float y, float z) {
		Lamp l = new Lamp();
		l.setX(x);
		l.setY(y);
		l.setZ(z);
		l.calculateLampPosInEyeSpace(mViewMatrix);
		l.sm.width = SHADOW_MAP_WIDTH;
		l.sm.height = SHADOW_MAP_HEIGHT;
		return l;
	}
	
	private void updateAndBindMatrices(GL2 gl2) {
		createMVMatrix();
		createMVPMatrix();
		bindMatrices(gl2);
	}
	
	private void sendLampsData(GL2 gl2) {
		gl2.glUniform1i(shaderProgram.getValueId("u_LampsCount"), lamps.length); // lamps count
		gl2.glUniform3f(shaderProgram.getValueId("u_ViewPos"), eyeX, eyeY, eyeZ); // current camera position
		for (int i = 0; i<lamps.length; i++) {
			gl2.glUniform1f(structLamps.getAmbientStrengthLocation(i), lamps[i].ambientStrength);
			gl2.glUniform1f(structLamps.getDiffuseStrengthLocation(i), lamps[i].diffuseStrength);
			gl2.glUniform1f(structLamps.getSpecularStrengthLocation(i), lamps[i].specularStrength);
			gl2.glUniform1f(structLamps.getKcLocation(i), lamps[i].kc);
			gl2.glUniform1f(structLamps.getKlLocation(i), lamps[i].kl);
			gl2.glUniform1f(structLamps.getKqLocation(i), lamps[i].kq);
			gl2.glUniform1i(structLamps.getShininessLocation(i), lamps[i].shininess);
			gl2.glUniform3f(structLamps.getLampPosLocation(i), lamps[i].getEyeX(), lamps[i].getEyeY(), lamps[i].getEyeZ());
			gl2.glUniform3f(structLamps.getLampColorLocation(i), lamps[i].getLightR(), lamps[i].getLightG(), lamps[i].getLightB());
			gl2.glUniformMatrix4fv(structLamps.getLightSpaceMatrixLocation(i), 1, false, lamps[i].sm.lightSpaceMatrix, 0);
		}
	}
	
	private void updateLampsLightModelMatrix(GL2 gl, float[] lightModelMatrix) {
		for (int i = 0; i<lamps.length; i++)
			gl.glUniformMatrix4fv(structLamps.getLightModelMatrixLocation(i), 1, false, lightModelMatrix, 0);
	}
	
	@Override
	public void dispose(GLAutoDrawable glAutoDrawable) {
		GL2 gl2 = glAutoDrawable.getGL().getGL2();
		shaderProgram.dispose(gl2);
	}

	private ByteBuffer[] shadowMapsBuffers = new ByteBuffer[LAMPS_COUNT];
	@Override
	public void display(GLAutoDrawable glAutoDrawable) {
		GL2 gl2 = glAutoDrawable.getGL().getGL2();
		
		gl2.glUseProgram(shaderShadowProgram.getProgramId());
		for (int i = 0; i<lamps.length; i++) {
			renderToDepthMap(gl2, shaderShadowProgram, lamps[i]);
			shadowMapsBuffers[i] = lamps[i].sm.pixels;
		}
		gl2.glUseProgram(0);
		
		gl2.glUseProgram(shaderProgram.getProgramId());
		gl2.glClear(GL2.GL_COLOR_BUFFER_BIT | GL2.GL_DEPTH_BUFFER_BIT);

		shadowMaps.pixels = TextureArray.createFullByteBuffer(shadowMapsBuffers);
		shadowMaps.write(gl2);
		TextureUtils.bindShadowMap(gl2, shaderProgram.getValueId("shadowMaps"), shadowMaps.get());
		render(gl2, shaderProgram);
		gl2.glUseProgram(0);
		
		//debug.renderTexture(gl2, ar.colorBuffers.get(1));
		//debug.renderTexture(gl2, ar.pingpongBuffers.get(0));
	}
	
	private void renderToDepthMap(GL2 gl, ShaderProgram shaderProgram, Lamp lamp) {
		lamp.sm.begin(gl);
		gl.glClear(GL.GL_DEPTH_BUFFER_BIT);
		
		VertexAttribTools.enable(gl, shaderProgram.getValueId("a_Position"));
		
		for (ModelObject model : models) {
			VertexAttribTools.pointer(gl, shaderProgram.getValueId("a_Position"), 3, model.vertexBuffer.rewind());
			GMatrix.setIdentityM(lamp.sm.modelMatrix, 0);
			GMatrix.translateM(lamp.sm.modelMatrix, 0, model.origin[0], model.origin[1], model.origin[2]);
			GMatrix.rotateM(lamp.sm.modelMatrix, 0, model.angles[0], 1, 0, 0);
			GMatrix.rotateM(lamp.sm.modelMatrix, 0, model.angles[1], 0, 1, 0);
			GMatrix.rotateM(lamp.sm.modelMatrix, 0, model.angles[2], 0, 0, 1);
			lamp.sm.updateMatrices();
			lamp.sm.updateData(gl, shaderProgram);
			for (Model.VerticesDescriptor vd : model.model.vd) gl.glDrawArrays(vd.POLYTYPE, vd.START, vd.END); // drawing
		}
		
		VertexAttribTools.disable(gl, shaderProgram.getValueId("a_Position"));
		
		lamp.sm.end(gl);
		
		lamp.writeShadowMapBuffer(gl);
	}
	
	private void render(GL2 gl, ShaderProgram shaderProgram) {
		ar.begin(gl);
		// view port to window size
		gl.glViewport(0, 0, WIN_W, WIN_H);
		// updating view matrix (because camera position was changed)
		createViewMatrix();
		// updating lamps eye space position
		for (int i = 0; i<lamps.length; i++) lamps[i].calculateLampPosInEyeSpace(mViewMatrix);
		// sending updated matrices to vertex shader
		GMatrix.setIdentityM(mModelMatrix, 0);
		updateAndBindMatrices(gl);
		// sending lamps positions to fragment shader
		sendLampsData(gl);
		
		VertexAttribTools.enable(gl, shaderProgram.getValueId("a_Position"), shaderProgram.getValueId("a_Normal"), shaderProgram.getValueId("a_Texture"));
		
		// drawing >>
		mapping.setNormalMappingEnabled(gl, 1); // with mapping
		VertexAttribTools.enable(gl, shaderProgram.getValueId("a_Tangent"), shaderProgram.getValueId("a_Bitangent"));

		for (ModelObject model : models) {
			mapping.bindADSNTextures(gl,
					model.ambientTexture, model.diffuseTexture, model.specularTexture, model.normalTexture);
			VertexAttribTools.pointer(gl, shaderProgram.getValueId("a_Position"), 3, model.vertexBuffer.rewind());
			VertexAttribTools.pointer(gl, shaderProgram.getValueId("a_Normal"), 3, model.normalsBuffer.rewind());
			VertexAttribTools.pointer(gl, shaderProgram.getValueId("a_Tangent"), 3, model.tangentsBuffer.rewind());
			VertexAttribTools.pointer(gl, shaderProgram.getValueId("a_Bitangent"), 3, model.bitangentsBuffer.rewind());
			VertexAttribTools.pointer(gl, shaderProgram.getValueId("a_Texture"), 2, model.texCoordsBuffer.rewind());
			GMatrix.setIdentityM(mModelMatrix, 0);
			GMatrix.translateM(mModelMatrix, 0, model.origin[0], model.origin[1], model.origin[2]);
			GMatrix.rotateM(mModelMatrix, 0, model.angles[0], 1, 0, 0);
			GMatrix.rotateM(mModelMatrix, 0, model.angles[1], 0, 1, 0);
			GMatrix.rotateM(mModelMatrix, 0, model.angles[2], 0, 0, 1);
			updateAndBindMatrices(gl);
			updateLampsLightModelMatrix(gl, mModelMatrix);
			for (Model.VerticesDescriptor vd : model.model.vd) gl.glDrawArrays(vd.POLYTYPE, vd.START, vd.END); // drawing
		}
		VertexAttribTools.disable(gl, shaderProgram.getValueId("a_Tangent"), shaderProgram.getValueId("a_Bitangent"));
		// <<
		
//		// drawing lamps >>
//		mapping.setNormalMappingEnabled(gl, 0);
//		mapping.bindADSTextures(gl,
//				lamp.ambientTexture, lamp.diffuseTexture, lamp.specularTexture);
//		
//		VertexAttribTools.pointer(gl, shaderProgram.getValueId("a_Position"), 3, lamp.vertexBuffer.rewind());
//		VertexAttribTools.pointer(gl, shaderProgram.getValueId("a_Normal"), 3, lamp.normalsBuffer.rewind());
//		VertexAttribTools.pointer(gl, shaderProgram.getValueId("a_Texture"), 2, lamp.texCoordsBuffer.rewind());
//		
//		for (int j = 0; j<lamps.length; j++) {
//			GMatrix.setIdentityM(mModelMatrix, 0);
//			GMatrix.translateM(mModelMatrix, 0, lamps[j].getWorldX(), lamps[j].getWorldY(), lamps[j].getWorldZ());
//			updateAndBindMatrices(gl);
//			// drawing model
//			for (Model.VerticesDescriptor vd : lamp.model.vd) gl.glDrawArrays(vd.POLYTYPE, vd.START, vd.END); // drawing
//		}
//		// <<
//		
		VertexAttribTools.disable(gl, shaderProgram.getValueId("a_Position"), shaderProgram.getValueId("a_Normal"), shaderProgram.getValueId("a_Texture"));

		ar.end(gl);
	}

	@Override
	public void reshape(GLAutoDrawable glAutoDrawable, int x, int y, int width, int height) {
		System.out.println("reshape");
		createProjectionMatrix(width, height);
		createMVPMatrix();
		createMVMatrix();
		WIN_W = width;
		WIN_H = height;
	}
	
	/**
	 * 
	 * Making blur and other effects here
	 * @author congard
	 *
	 */
	class AdvancedRendering {
		private ShaderProgram shaderBlurProgram, shaderBlendProgram;
		private IntBuffer displayFBO = IntBuffer.allocate(1);
		private IntBuffer pingpongFBO = IntBuffer.allocate(2);
		private IntBuffer colorBuffers = IntBuffer.allocate(2);
		private IntBuffer pingpongBuffers = IntBuffer.allocate(2);
		private IntBuffer rboBuffer = IntBuffer.allocate(1);
		private FloatBuffer vertexBuffer, texCoordsBuffer;
		private int[] attachments = { GL2.GL_COLOR_ATTACHMENT0, GL2.GL_COLOR_ATTACHMENT1 };
		private int blurAmount = 10;
		private boolean horizontal = true, firstIteration = true; // blur variables
		private float exposure = 3, gamma = 1.2f; // blend variables
				
		public void init(GL2 gl) {
			// creating buffers and textures
			gl.glGenFramebuffers(1, displayFBO);
			gl.glGenTextures(2, colorBuffers);
			gl.glGenRenderbuffers(1, rboBuffer);
			gl.glGenFramebuffers(2, pingpongFBO);
			gl.glGenTextures(2, pingpongBuffers);
			
			// configuring textures
			gl.glActiveTexture(GL.GL_TEXTURE0);
			for (int i = 0; i < 2; i++) {
				// configuring render textures
				gl.glBindFramebuffer(GL.GL_FRAMEBUFFER, displayFBO.get(0));
				gl.glBindTexture(GL.GL_TEXTURE_2D, colorBuffers.get(i));
				applyDefaultTextureParams(gl);
				// attach texture to framebuffer
				gl.glFramebufferTexture2D(GL.GL_FRAMEBUFFER, GL2.GL_COLOR_ATTACHMENT0 + i, GL.GL_TEXTURE_2D, colorBuffers.get(i), 0);
				
				// configuring ping-pong (blur) textures
				gl.glBindFramebuffer(GL.GL_FRAMEBUFFER, pingpongFBO.get(i));
				gl.glBindTexture(GL.GL_TEXTURE_2D, pingpongBuffers.get(i));
				applyDefaultTextureParams(gl);
				// attach texture to framebuffer
				gl.glFramebufferTexture2D(GL.GL_FRAMEBUFFER, GL2.GL_COLOR_ATTACHMENT0, GL.GL_TEXTURE_2D, pingpongBuffers.get(i), 0);
			}
			gl.glBindFramebuffer(GL.GL_FRAMEBUFFER, 0);
			
			// loading blur shaders
			shaderBlurProgram = new ShaderProgram();
			if (!shaderBlurProgram.init(gl, new File("./resources/shaders/vertex_blur_shader.glsl"), new File("./resources/shaders/fragment_blur_shader.glsl")))
				throw new IllegalStateException("Unable to initiate the shaders!");
				
			shaderBlurProgram.loadValuesIds(gl, 
					new GLSLValue("inPos", GLSLValue.ATTRIB),
					new GLSLValue("inTexCoords", GLSLValue.ATTRIB),
					new GLSLValue("image", GLSLValue.UNIFORM),
					new GLSLValue("isHorizontal", GLSLValue.UNIFORM)
			);
			
			// creating float buffers for quad rendering
			float[] vertices = {
					-1,  1, 0,
		            -1, -1, 0,
		             1,  1, 0,
		             1, -1, 0,
			}, texCoords = {
					0, 1,
					0, 0,
					1, 1,
					1, 0,
			};
			
			vertexBuffer = Buffers.newDirectFloatBuffer(vertices.length);
			texCoordsBuffer = Buffers.newDirectFloatBuffer(texCoords.length);
			vertexBuffer.put(vertices).position(0);
			texCoordsBuffer.put(texCoords).position(0);
			
			// loading blend shaders
			shaderBlendProgram = new ShaderProgram();
			if (!shaderBlendProgram.init(gl, new File("./resources/shaders/vertex_blend_shader.glsl"), new File("./resources/shaders/fragment_blend_shader.glsl")))
				throw new IllegalStateException("Unable to initiate the shaders!");
				
			shaderBlendProgram.loadValuesIds(gl, 
					new GLSLValue("inPos", GLSLValue.ATTRIB),
					new GLSLValue("inTexCoords", GLSLValue.ATTRIB),
					new GLSLValue("scene", GLSLValue.UNIFORM),
					new GLSLValue("bluredScene", GLSLValue.UNIFORM),
					new GLSLValue("exposure", GLSLValue.UNIFORM),
					new GLSLValue("gamma", GLSLValue.UNIFORM)
			);
			
			gl.glUseProgram(shaderBlendProgram.getProgramId());
			gl.glUniform1i(shaderBlendProgram.getValueId("scene"), 0); // GL_TEXTURE0
			gl.glUniform1i(shaderBlendProgram.getValueId("bluredScene"), 1); // GL_TEXTURE1
			gl.glUniform1f(shaderBlendProgram.getValueId("exposure"), exposure);
			gl.glUniform1f(shaderBlendProgram.getValueId("gamma"), gamma);
			gl.glUseProgram(0);
		}
		
		// bind to custom buffers for draw into texture with MRT
		public void begin(GL2 gl) {
			gl.glBindFramebuffer(GL.GL_FRAMEBUFFER, displayFBO.get(0));
			gl.glBindRenderbuffer(GL.GL_RENDERBUFFER, rboBuffer.get(0));
		    gl.glRenderbufferStorage(GL.GL_RENDERBUFFER, GL2.GL_DEPTH_COMPONENT, WIN_W, WIN_H);
		    gl.glFramebufferRenderbuffer(GL.GL_FRAMEBUFFER, GL.GL_DEPTH_ATTACHMENT, GL.GL_RENDERBUFFER, rboBuffer.get(0));
		    gl.glDrawBuffers(2, attachments, 0); // use 2 color components
		    gl.glClear(GL.GL_COLOR_BUFFER_BIT | GL.GL_DEPTH_BUFFER_BIT); // clear it
		    // configuring textures
			gl.glActiveTexture(GL.GL_TEXTURE0);
			for (int i = 0; i < 2; i++) {
				gl.glBindTexture(GL.GL_TEXTURE_2D, colorBuffers.get(i));
				gl.glTexImage2D(GL.GL_TEXTURE_2D, 0, GL.GL_RGB16F, WIN_W, WIN_H, 0, GL.GL_RGB, GL.GL_FLOAT, null);
			}
		}
		
		// bind to default buffers && draw
		public void end(GL2 gl) {
			gl.glBindRenderbuffer(GL.GL_RENDERBUFFER, 0);
			gl.glDrawBuffers(1, attachments, 0); // use 1 color component
			mkBlur(gl);
			gl.glBindFramebuffer(GL.GL_FRAMEBUFFER, 0);
			blend(gl);
			gl.glUseProgram(0);
		}
		
		private void mkBlur(GL2 gl) {
			// configuring textures
			gl.glActiveTexture(GL.GL_TEXTURE0);
			for (int i = 0; i < 2; i++) {
				gl.glBindFramebuffer(GL.GL_FRAMEBUFFER, pingpongFBO.get(i));
				gl.glBindTexture(GL.GL_TEXTURE_2D, pingpongBuffers.get(i));
				gl.glTexImage2D(GL.GL_TEXTURE_2D, 0, GL.GL_RGB16F, WIN_W, WIN_H, 0, GL.GL_RGB, GL.GL_FLOAT, null);
				// Do not need to call glClear, because the texture is completely redrawn
			}
			
			horizontal = true; 
			firstIteration = true;
			gl.glUseProgram(shaderBlurProgram.getProgramId());
			gl.glActiveTexture(GL.GL_TEXTURE0);
			for (int i = 0; i < blurAmount; i++) {
				gl.glBindFramebuffer(GL.GL_FRAMEBUFFER, pingpongFBO.get(bool2int(horizontal)));
				gl.glUniform1i(shaderBlurProgram.getValueId("isHorizontal"), bool2int(horizontal));
				gl.glBindTexture(GL.GL_TEXTURE_2D, firstIteration ? colorBuffers.get(1) : pingpongBuffers.get(bool2int(!horizontal)));
				renderQuad(gl, shaderBlurProgram);
				horizontal = !horizontal;
				if (firstIteration) firstIteration = false;
			}
		}
		
		private void blend(GL2 gl) {
			// Do not need to call glClear, because the texture is completely redrawn
			gl.glUseProgram(shaderBlendProgram.getProgramId());
			gl.glActiveTexture(GL2.GL_TEXTURE0);
			gl.glBindTexture(GL.GL_TEXTURE_2D, colorBuffers.get(0));
			gl.glActiveTexture(GL2.GL_TEXTURE1);
			gl.glBindTexture(GL.GL_TEXTURE_2D, pingpongBuffers.get(bool2int(!horizontal)));
			renderQuad(gl, shaderBlendProgram);
		}
		
		private void applyDefaultTextureParams(GL2 gl) {
			gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MIN_FILTER, GL.GL_LINEAR);
			gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MAG_FILTER, GL.GL_LINEAR);
			gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_WRAP_S, GL.GL_CLAMP_TO_EDGE);
			gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_WRAP_T, GL.GL_CLAMP_TO_EDGE);
		}
		
		private int bool2int(boolean bool) {
			return bool ? 1 : 0;
		}
		
		private void renderQuad(GL2 gl, ShaderProgram shaderProgram) {
			VertexAttribTools.enable(gl, shaderProgram.getValueId("inPos"), shaderProgram.getValueId("inTexCoords"));
			VertexAttribTools.pointer(gl, shaderProgram.getValueId("inPos"), 3, vertexBuffer.rewind());
			VertexAttribTools.pointer(gl, shaderProgram.getValueId("inTexCoords"), 2, texCoordsBuffer.rewind());
			
			gl.glDrawArrays(GL.GL_TRIANGLE_STRIP, 0, 4);
			
			VertexAttribTools.disable(gl, shaderProgram.getValueId("inPos"), shaderProgram.getValueId("inTexCoords"));
		}
	}
}