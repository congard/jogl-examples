package example.congard.jogl.example015.cmfanimatedscene;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;

import com.jogamp.common.nio.Buffers;
import com.jogamp.opengl.GL;
import com.jogamp.opengl.GL2;
import com.jogamp.opengl.GL3;
import com.jogamp.opengl.GLAutoDrawable;
import com.jogamp.opengl.GLEventListener;

import example.congard.jogl.example015.cmfanimatedscene.ShaderProgram.GLSLValue;
import example.congard.jogl.example015.cmfanimatedscene.TextureUtils.Mapping;
import example.congard.jogl.example015.cmfanimatedscene.TextureUtils.TextureArray;
import lib.congard.basemodelmaker.Timer;
import lib.congard.cmf.loader.CMFLoader;
import lib.congard.cmf2arrays.CMF2Arrays;
import lib.congard.cmf2arrays.Polygon;
import lib.congard.ml.geometry.Geometry;
import lib.congard.ml.graphics.GMatrix;
import lib.congard.ml.graphics.GaussianBlur;

/**
 * Rendering with point lighting + CMF models + animated scene + lamp object
 * Only for triangulated models
 * 
 * @author congard
 * 
 * Links:
 * I:
 * t.me/congard
 * gitlab.com/congard
 * dbcongard@gmail.com
 * 
 * serhiy:
 * github.com/serhiy
 */
public class Renderer implements GLEventListener {
	/* --- constants --- */
	private final static int LAMPS_COUNT = 1; // 1 lamp on scene
	private final static int SHADOW_MAP_WIDTH = 1024, SHADOW_MAP_HEIGHT = 1024;
	
	private Lamp[] lamps;
	private ShaderProgram shaderProgram, shaderShadowProgram;
	private float[] mProjectionMatrix = new float[16];
	private float[] mViewMatrix = new float[16];
	private float[] mModelMatrix = new float[16];
	private float[][] rotationAnglesRad;
	private float eyeX = 0, eyeY = 10, eyeZ = 10, // camera position
			lookX = 0, lookY = 4, lookZ = 0,  // look point
			upX = 0, upY = 1, upZ = 0; // look at the axis y
	private Lamps structLamps;
	private ModelObject cube, floor, rapier, lucy, jplamp;
	private Mapping mapping;
	private TextureArray shadowMaps;
	private ModelObject[] models = new ModelObject[4], molamps = new ModelObject[LAMPS_COUNT];
	private AdvancedRendering ar = new AdvancedRendering();
	/* --- for measuring the operating time --- */
	private Timer timer = new Timer();
	
	/* --- window size --- */
	private int WIN_W = 1024, WIN_H = 1024;
	
	/* --- fps meter --- */
	private int frame = 0, frameTime;
	private final int frames = 256;
	private float fps = 0;
	private long time = 0, startTime;
	
	@Override
	public void init(GLAutoDrawable glAutoDrawable) {
		GL2 gl2 = glAutoDrawable.getGL().getGL2();
		GL3 gl3 = glAutoDrawable.getGL().getGL3();

		File vertexShader = new File("./resources/shaders/vertex_shader.glsl");
		File fragmentShader = new File("./resources/shaders/fragment_shader.glsl");
		File vertexShadowShader = new File("./resources/shaders/vertex_shadow_shader.glsl");
		File fragmentShadowShader = new File("./resources/shaders/fragment_shadow_shader.glsl");
		File geometryShadowShader = new File("./resources/shaders/geometry_shadow_shader.glsl");
		
		timer.start("Loading shaders");
		
		shaderProgram = new ShaderProgram();
		if (!shaderProgram.init(gl2, vertexShader, fragmentShader)) {
			throw new IllegalStateException("Unable to initiate the shaders!");
		}
		
		shaderShadowProgram = new ShaderProgram();
		if (!shaderShadowProgram.init(gl2, vertexShadowShader, fragmentShadowShader, geometryShadowShader)) {
			throw new IllegalStateException("Unable to initiate the shaders!");
		}
		
		shaderProgram.loadValuesIds(gl2, 
				new GLSLValue("a_Position", GLSLValue.ATTRIB),
				new GLSLValue("a_Normal", GLSLValue.ATTRIB),
				new GLSLValue("a_Texture", GLSLValue.ATTRIB),
				new GLSLValue("a_Tangent", GLSLValue.ATTRIB),
				new GLSLValue("a_Bitangent", GLSLValue.ATTRIB),
				new GLSLValue("u_LampsCount", GLSLValue.UNIFORM),
				new GLSLValue("u_ViewPos", GLSLValue.UNIFORM),
				new GLSLValue("shadowMaps", GLSLValue.UNIFORM),
				new GLSLValue("u_Model", GLSLValue.UNIFORM),
				new GLSLValue("u_View", GLSLValue.UNIFORM),
				new GLSLValue("u_Projection", GLSLValue.UNIFORM),
				new GLSLValue("far_plane", GLSLValue.UNIFORM)
		);
		
		shaderShadowProgram.loadValuesIds(gl2, 
				new GLSLValue("a_Position", GLSLValue.ATTRIB),
				new GLSLValue("u_ModelMatrix", GLSLValue.UNIFORM),
				new GLSLValue("lightPos", GLSLValue.UNIFORM),
				new GLSLValue("far_plane", GLSLValue.UNIFORM)
		);
		
		timer.stop("Shaders loaded in %time%");
		
		mapping = new Mapping(gl2, shaderProgram.getProgramId());
		mapping.loadLocations();
		
		timer.start("Loading models");
		
		/* --- configuring types --- */
		Polygon.TYPE_TRIANGLE = GL2.GL_TRIANGLES;
		Polygon.TYPE_QUAD = GL2.GL_QUADS;
		Polygon.TYPE_POLYGON = GL2.GL_POLYGON;
		// loading cmf models
		cube = new ModelObject(loadModel(new File("./resources/models/cube/cube_triangulated.cmf")), 
				TextureUtils.loadTexture(gl2, new File("./resources/textures/cube/cube_ambient.jpg")),
				TextureUtils.loadTexture(gl2, new File("./resources/textures/cube/cube_diffuse.jpg")),
				TextureUtils.loadTexture(gl2, new File("./resources/textures/cube/cube_specular.jpg")),
				TextureUtils.loadTexture(gl2, new File("./resources/textures/cube/cube_normal.jpg")));
		
		floor = new ModelObject(loadModel(new File("./resources/models/brickwall/brickwall_triangulated.cmf")), 
				TextureUtils.loadTexture(gl2, new File("./resources/textures/brickwall/brickwall.jpg")),
				TextureUtils.loadTexture(gl2, new File("./resources/textures/brickwall/brickwall_normal.jpg")));
		
		rapier = new ModelObject(loadModel(new File("./resources/models/italian_rapier/italian_rapier_triangulated.cmf")));
		rapier.ambientTexture = TextureUtils.loadTexture(gl2, new File("./resources/textures/italian_rapier/diffuse.png"));
		rapier.diffuseTexture = rapier.ambientTexture;
		rapier.specularTexture = TextureUtils.loadTexture(gl2, new File("./resources/textures/italian_rapier/specular.png"));
		rapier.normalTexture = TextureUtils.loadTexture(gl2, new File("./resources/textures/italian_rapier/normal.png"));
		
		lucy = new ModelObject(loadModel(new File("./resources/models/LucyAngel/Stanfords_Lucy_Angel_triangulated.cmf")));
		lucy.ambientTexture = TextureUtils.loadTexture(gl2, new File("./resources/textures/LucyAngel/Stanfords_Lucy_Angel_diffuse.jpg"));
		lucy.diffuseTexture = lucy.ambientTexture;
		lucy.specularTexture = TextureUtils.loadTexture(gl2, new File("./resources/textures/LucyAngel/Stanfords_Lucy_Angel_specular.jpg"));;
		lucy.normalTexture = TextureUtils.loadTexture(gl2, new File("./resources/textures/LucyAngel/Stanfords_Lucy_Angel_normal.jpg"));
		
		jplamp = new ModelObject(loadModel(new File("./resources/models/japanese_lamp/japanese_lamp.cmf"), true), 
				TextureUtils.loadTexture(gl2, new File("./resources/textures/japanese_lamp/ambient.tga")),
				TextureUtils.loadTexture(gl2, new File("./resources/textures/japanese_lamp/diffuse.tga")),
				TextureUtils.loadTexture(gl2, new File("./resources/textures/japanese_lamp/specular.tga")),
				TextureUtils.loadTexture(gl2, new File("./resources/textures/japanese_lamp/normal.tga")));
		
		floor.angles[0] = -90;
		cube.angles[0] = 0;
		rapier.angles[0] = 90;
		rapier.origin[2] = 3;
		rapier.origin[1] = 3;
		lucy.origin[1] = 2.5f;
		
		// making tangents and bitangents buffers
		// only for TRIANGULATED models
		cube.mkTBBuffers();
		floor.mkTBBuffers();
		rapier.mkTBBuffers();
		lucy.mkTBBuffers();
		jplamp.mkTBBuffers();
		
		models[0] = cube;
		models[1] = floor;
		models[2] = rapier;
		models[3] = lucy;
		
		molamps[0] = jplamp;
		
		timer.stop("Models loaded in %time%");
		
		gl2.glEnable(GL2.GL_DEPTH_TEST);
		gl2.glDepthMask(true);
		gl2.glClearColor(0, 0, 0, 1);
		
		createMatrices();
		
		// bloom init
		ar.init(gl2);
		
		structLamps = new Lamps(gl2, shaderProgram, LAMPS_COUNT);
		structLamps.loadLocations();
		lamps = new Lamp[] {
				createLamp(0, 8, 8).setColor(1, 1, 1).setAmbientStrength(0.01f).setDiffuseStrength(1).setSpecularStrength(1f).setShininess(32).setAttenuationTerms(1, 0.045f, 0.0075f).initShadowMapping(gl3)
		};
		for (int i = 0; i < LAMPS_COUNT; i++) molamps[i].origin = lamps[i].mLightPosInWorldSpace; // lamp object to lamp position
		
		shadowMaps = new TextureArray(10, LAMPS_COUNT);
		shadowMaps.init(gl2, shaderProgram.getProgramId(), "shadowMaps");
		for (int i = 0; i < lamps.length; i++) {
			shadowMaps.addTexture(i, lamps[i].sm.depthCubemap.get(0));
			lamps[i].sm.initShadowMatrices(gl2, shaderShadowProgram.getProgramId(), "shadowMatrices");
		}
		shadowMaps.bind(gl2, shaderProgram.getProgramId());
		
		// this array need only for animation
		rotationAnglesRad = new float[][] {
			new float[] { (float)Math.sin(Math.toRadians(0.1)), (float)Math.cos(Math.toRadians(0.1)) }, // for lamp
			new float[] { (float)Math.sin(Math.toRadians(0.01)), (float)Math.cos(Math.toRadians(0.01)) } // for camera
		};

		// animation thread
		new Thread(new Runnable() {
			float[] cameraPos, lampPos;
			
			@Override
			public void run() {
				while (true) {
					try {
						Thread.sleep(4);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					
					lampPos = Geometry.rotateAroundYf(0, 0, 0, lamps[0].mLightPosInWorldSpace[0], lamps[0].mLightPosInWorldSpace[1], lamps[0].mLightPosInWorldSpace[2], rotationAnglesRad[0][0], rotationAnglesRad[0][1]);
					cameraPos = Geometry.rotateAroundYf(0, 0, 0, eyeX, eyeY, eyeZ, rotationAnglesRad[1][0], rotationAnglesRad[1][1]);
					
					eyeX = cameraPos[0];
					eyeY = cameraPos[1];
					eyeZ = cameraPos[2];
					lamps[0].mLightPosInWorldSpace[0] = lampPos[0];
					lamps[0].mLightPosInWorldSpace[1] = Math.abs(lampPos[0]) + 4;
					lamps[0].mLightPosInWorldSpace[2] = lampPos[2];
				}
			}
		}).start();
	}
	
	/**
	 * Loads CMF model, inverseNormals = false
	 * @param path
	 * @return
	 */
	private static CMF2Arrays loadModel(File path) {
		return loadModel(path, false);
	}
	
	/**
	 * Loads CMF model
	 * @param path
	 * @param inverseNormals
	 * @return
	 */
	private static CMF2Arrays loadModel(File path, boolean inverseNormals) {
		CMFLoader loader = new CMFLoader();
		try {
			loader.loadCMF(path);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		loader.load();
		
		if (inverseNormals) ModelObject.inverseNormals(loader.normals);
		
		CMF2Arrays arrays = new CMF2Arrays();
		arrays.init(loader.vertices, loader.normals, loader.texCoords, loader.faces);
		arrays.make(true, true, true);
		return arrays;
	}
	
	private void createMatrices() {
		createProjectionMatrix(1, 1);
		createViewMatrix();
		createModelMatrix();
		System.out.println("Matrices created");
	}
	
	private void createProjectionMatrix(int width, int height) {
		float ratio = 1;
        float left = -1;
        float right = 1;
        float bottom = -1;
        float top = 1;
        float near = 1, far = 32;
        if (width > height) {
            ratio = (float) width / (float) height;
            left *= ratio;
            right *= ratio;
        } else {
            ratio = (float) height / (float) width;
            bottom *= ratio;
            top *= ratio;
        }

        GMatrix.frustumM(mProjectionMatrix, 0, left, right, bottom, top, near, far);
        //GMatrix.perspectiveM(mProjectionMatrix, Geometry.toRadiansf(90), ratio, near, far);
	}
	
	private void createViewMatrix() {
		GMatrix.setLookAtM(mViewMatrix, 0, eyeX, eyeY, eyeZ, lookX, lookY, lookZ, upX, upY, upZ);
	}
	
	private void createModelMatrix() {
		GMatrix.setIdentityM(mModelMatrix, 0);
	}
	
	private Lamp createLamp(float x, float y, float z) {
		Lamp l = new Lamp();
		l.setX(x);
		l.setY(y);
		l.setZ(z);
		l.sm.width = SHADOW_MAP_WIDTH;
		l.sm.height = SHADOW_MAP_HEIGHT;
		return l;
	}
	
	private void sendLampsData(GL2 gl2) {
		gl2.glUniform1i(shaderProgram.getValueId("u_LampsCount"), lamps.length); // lamps count
		gl2.glUniform3f(shaderProgram.getValueId("u_ViewPos"), eyeX, eyeY, eyeZ); // current camera position
		gl2.glUniform1f(shaderProgram.getValueId("far_plane"), Lamp.FAR_PLANE); // far plane in shadow matrices
		for (int i = 0; i < lamps.length; i++) {
			gl2.glUniform1f(structLamps.getAmbientStrengthLocation(i), lamps[i].ambientStrength);
			gl2.glUniform1f(structLamps.getDiffuseStrengthLocation(i), lamps[i].diffuseStrength);
			gl2.glUniform1f(structLamps.getSpecularStrengthLocation(i), lamps[i].specularStrength);
			gl2.glUniform1f(structLamps.getKcLocation(i), lamps[i].kc);
			gl2.glUniform1f(structLamps.getKlLocation(i), lamps[i].kl);
			gl2.glUniform1f(structLamps.getKqLocation(i), lamps[i].kq);
			gl2.glUniform1i(structLamps.getShininessLocation(i), lamps[i].shininess);
			gl2.glUniform3f(structLamps.getLampPosLocation(i), lamps[i].getX(), lamps[i].getY(), lamps[i].getZ());
			gl2.glUniform3f(structLamps.getLampColorLocation(i), lamps[i].getLightR(), lamps[i].getLightG(), lamps[i].getLightB());
		}
	}
	
	@Override
	public void dispose(GLAutoDrawable glAutoDrawable) {
		GL2 gl2 = glAutoDrawable.getGL().getGL2();
		shaderProgram.dispose(gl2);
	}

	@Override
	public void display(GLAutoDrawable glAutoDrawable) {
		startTime = System.currentTimeMillis();
		
		GL2 gl = glAutoDrawable.getGL().getGL2();
		
		/* --- depth map --- */
		gl.glUseProgram(shaderShadowProgram.getProgramId());
		gl.glUniform1f(shaderShadowProgram.getValueId("far_plane"), Lamp.FAR_PLANE);
		for (int i = 0; i<lamps.length; i++) renderToDepthMap(gl, shaderShadowProgram, i);
		gl.glUseProgram(0);
		
		/* --- color rendering --- */
		gl.glUseProgram(shaderProgram.getProgramId());
		gl.glClear(GL2.GL_COLOR_BUFFER_BIT | GL2.GL_DEPTH_BUFFER_BIT);
		render(gl, shaderProgram);
		gl.glUseProgram(0);
		
		frameTime = (int) (System.currentTimeMillis() - startTime);
		time += frameTime;
		fps += 1000f / frameTime;
		if (frame >= frames) {
			System.out.println(frame + " frames in " + time + " ms. FPS = " + (fps / frames));
			frame = 0;
			time = 0;
			fps = 0;
		} else frame++;
	}
	
	private void renderToDepthMap(GL2 gl, ShaderProgram shaderProgram, int index) {
		lamps[index].sm.begin(gl);
		lamps[index].sm.setLightPos(gl, shaderProgram.getValueId("lightPos"));
		lamps[index].sm.updateMatrices();
		lamps[index].sm.setShadowMatrices(gl);
		gl.glClear(GL.GL_DEPTH_BUFFER_BIT);
		
		VertexAttribTools.enable(gl, shaderProgram.getValueId("a_Position"));
		
		// drawing models
		for (ModelObject model : models) drawModelDM(gl, shaderProgram, model);
		
		// drawing lamps
		for (int i = 0; i < molamps.length; i++) {
			if (i == index) continue;
			drawModelDM(gl, shaderProgram, molamps[i]);
		}
		
		VertexAttribTools.disable(gl, shaderProgram.getValueId("a_Position"));
		
		lamps[index].sm.end(gl);
	}
	
	private void render(GL2 gl, ShaderProgram shaderProgram) {
		ar.begin(gl);
		// view port to window size
		gl.glViewport(0, 0, WIN_W, WIN_H);
		// updating view matrix (because camera position was changed)
		createViewMatrix();
		// sending updated matrices to vertex shader
		ShaderUtils.setMatrix(gl, shaderProgram.getValueId("u_View"), mViewMatrix);
		ShaderUtils.setMatrix(gl, shaderProgram.getValueId("u_Projection"), mProjectionMatrix);
		// sending lamps parameters to fragment shader
		sendLampsData(gl);
		
		VertexAttribTools.enable(gl, shaderProgram.getValueId("a_Position"), shaderProgram.getValueId("a_Normal"), shaderProgram.getValueId("a_Texture"));
		
		// drawing
		mapping.setNormalMappingEnabled(gl, 1); // with mapping
		VertexAttribTools.enable(gl, shaderProgram.getValueId("a_Tangent"), shaderProgram.getValueId("a_Bitangent"));

		drawModels(gl, shaderProgram, models, molamps);
		
		VertexAttribTools.disable(gl, shaderProgram.getValueId("a_Position"), shaderProgram.getValueId("a_Normal"), shaderProgram.getValueId("a_Texture"), shaderProgram.getValueId("a_Tangent"), shaderProgram.getValueId("a_Bitangent"));
		
		ar.end(gl);
	}
	
	/**
	 * Draws model in depth map
	 * @param gl
	 * @param shaderProgram
	 * @param model
	 */
	private void drawModelDM(GL2 gl, ShaderProgram shaderProgram, ModelObject model) {
		VertexAttribTools.pointer(gl, shaderProgram.getValueId("a_Position"), 3, model.vertexBuffer.rewind());
		GMatrix.setIdentityM(mModelMatrix, 0);
		GMatrix.translateM(mModelMatrix, 0, model.origin[0], model.origin[1], model.origin[2]);
		GMatrix.rotateM(mModelMatrix, 0, model.angles[0], 1, 0, 0);
		GMatrix.rotateM(mModelMatrix, 0, model.angles[1], 0, 1, 0);
		GMatrix.rotateM(mModelMatrix, 0, model.angles[2], 0, 0, 1);
		ShaderUtils.setMatrix(gl, shaderProgram.getValueId("u_ModelMatrix"), mModelMatrix);
		for (Polygon p : model.cmfarrays.polygons) gl.glDrawArrays(p.type, p.start, p.count); // drawing
	}
	
	/**
	 * Draws models from ModelObject 2D array
	 * @param gl
	 * @param shaderProgram
	 * @param modelsArray
	 */
	private void drawModels(GL2 gl, ShaderProgram shaderProgram, ModelObject[]...modelsArray) {
		for (ModelObject[] models : modelsArray) {
			for (ModelObject model : models) {
				mapping.bindADSNTextures(gl,
						model.ambientTexture, model.diffuseTexture, model.specularTexture, model.normalTexture);
				VertexAttribTools.pointer(gl, shaderProgram.getValueId("a_Position"), 3, model.vertexBuffer.rewind());
				VertexAttribTools.pointer(gl, shaderProgram.getValueId("a_Normal"), 3, model.normalsBuffer.rewind());
				VertexAttribTools.pointer(gl, shaderProgram.getValueId("a_Tangent"), 3, model.tangentsBuffer.rewind());
				VertexAttribTools.pointer(gl, shaderProgram.getValueId("a_Bitangent"), 3, model.bitangentsBuffer.rewind());
				VertexAttribTools.pointer(gl, shaderProgram.getValueId("a_Texture"), 2, model.texCoordsBuffer.rewind());
				GMatrix.setIdentityM(mModelMatrix, 0);
				GMatrix.translateM(mModelMatrix, 0, model.origin[0], model.origin[1], model.origin[2]);
				GMatrix.rotateM(mModelMatrix, 0, model.angles[0], 1, 0, 0);
				GMatrix.rotateM(mModelMatrix, 0, model.angles[1], 0, 1, 0);
				GMatrix.rotateM(mModelMatrix, 0, model.angles[2], 0, 0, 1);
				ShaderUtils.setMatrix(gl, shaderProgram.getValueId("u_Model"), mModelMatrix);
				for (Polygon p : model.cmfarrays.polygons) gl.glDrawArrays(p.type, p.start, p.count); // drawing
			}
		}
	}

	@Override
	public void reshape(GLAutoDrawable glAutoDrawable, int x, int y, int width, int height) {
		createProjectionMatrix(width, height);
		WIN_W = width;
		WIN_H = height;
	}
	
	/**
	 * 
	 * Making blur and other effects here
	 * @author congard
	 *
	 */
	class AdvancedRendering {
		private ShaderProgram shaderBlurProgram, shaderBlendProgram;
		private IntBuffer displayFBO = IntBuffer.allocate(1);
		private IntBuffer pingpongFBO = IntBuffer.allocate(2);
		private IntBuffer colorBuffers = IntBuffer.allocate(2);
		private IntBuffer pingpongBuffers = IntBuffer.allocate(2);
		private IntBuffer rboBuffer = IntBuffer.allocate(1);
		private FloatBuffer vertexBuffer, texCoordsBuffer;
		private int[] attachments = { GL2.GL_COLOR_ATTACHMENT0, GL2.GL_COLOR_ATTACHMENT1 };
		private int blurAmount = 4;
		private boolean horizontal = true, firstIteration = true; // blur variables
		private float exposure = 3, gamma = 1f; // blend variables
				
		public void init(GL2 gl) {
			// creating buffers and textures
			gl.glGenFramebuffers(1, displayFBO);
			gl.glGenTextures(2, colorBuffers);
			gl.glGenRenderbuffers(1, rboBuffer);
			gl.glGenFramebuffers(2, pingpongFBO);
			gl.glGenTextures(2, pingpongBuffers);
			
			// configuring textures
			gl.glActiveTexture(GL.GL_TEXTURE0);
			for (int i = 0; i < 2; i++) {
				// configuring render textures
				gl.glBindFramebuffer(GL.GL_FRAMEBUFFER, displayFBO.get(0));
				gl.glBindTexture(GL.GL_TEXTURE_2D, colorBuffers.get(i));
				applyDefaultTextureParams(gl);
				// attach texture to framebuffer
				gl.glFramebufferTexture2D(GL.GL_FRAMEBUFFER, GL2.GL_COLOR_ATTACHMENT0 + i, GL.GL_TEXTURE_2D, colorBuffers.get(i), 0);
				
				// configuring ping-pong (blur) textures
				gl.glBindFramebuffer(GL.GL_FRAMEBUFFER, pingpongFBO.get(i));
				gl.glBindTexture(GL.GL_TEXTURE_2D, pingpongBuffers.get(i));
				applyDefaultTextureParams(gl);
				// attach texture to framebuffer
				gl.glFramebufferTexture2D(GL.GL_FRAMEBUFFER, GL2.GL_COLOR_ATTACHMENT0, GL.GL_TEXTURE_2D, pingpongBuffers.get(i), 0);
			}
			gl.glBindFramebuffer(GL.GL_FRAMEBUFFER, 0);
			
			// loading blur shaders
			shaderBlurProgram = new ShaderProgram();
			if (!shaderBlurProgram.init(gl, new File("./resources/shaders/vertex_blur_shader.glsl"), new File("./resources/shaders/fragment_blur_shader.glsl")))
				throw new IllegalStateException("Unable to initiate the shaders!");
				
			shaderBlurProgram.loadValuesIds(gl, 
					new GLSLValue("inPos", GLSLValue.ATTRIB),
					new GLSLValue("inTexCoords", GLSLValue.ATTRIB),
					new GLSLValue("image", GLSLValue.UNIFORM),
					new GLSLValue("isHorizontal", GLSLValue.UNIFORM),
					new GLSLValue("kernel", GLSLValue.UNIFORM)
			);
			
			// configuring blur kernel
			// sending to shader center of kernel and right part
			float[] kernel = GaussianBlur.get1DKernelf(15, 6);
			gl.glUseProgram(shaderBlurProgram.getProgramId());
			for (int i = 0; i < kernel.length / 2 + 1; i++) gl.glUniform1f(gl.glGetUniformLocation(shaderBlurProgram.getProgramId(), "kernel[" + i + "]"), kernel[kernel.length / 2 + i]);
			gl.glUseProgram(0);
			
			// creating float buffers for quad rendering
			float[] vertices = {
					-1,  1, 0,
		            -1, -1, 0,
		             1,  1, 0,
		             1, -1, 0,
			}, texCoords = {
					0, 1,
					0, 0,
					1, 1,
					1, 0,
			};
			
			vertexBuffer = Buffers.newDirectFloatBuffer(vertices.length);
			texCoordsBuffer = Buffers.newDirectFloatBuffer(texCoords.length);
			vertexBuffer.put(vertices).position(0);
			texCoordsBuffer.put(texCoords).position(0);
			
			// loading blend shaders
			shaderBlendProgram = new ShaderProgram();
			if (!shaderBlendProgram.init(gl, new File("./resources/shaders/vertex_blend_shader.glsl"), new File("./resources/shaders/fragment_blend_shader.glsl")))
				throw new IllegalStateException("Unable to initiate the shaders!");
				
			shaderBlendProgram.loadValuesIds(gl, 
					new GLSLValue("inPos", GLSLValue.ATTRIB),
					new GLSLValue("inTexCoords", GLSLValue.ATTRIB),
					new GLSLValue("scene", GLSLValue.UNIFORM),
					new GLSLValue("bluredScene", GLSLValue.UNIFORM),
					new GLSLValue("exposure", GLSLValue.UNIFORM),
					new GLSLValue("gamma", GLSLValue.UNIFORM)
			);
			
			gl.glUseProgram(shaderBlendProgram.getProgramId());
			gl.glUniform1i(shaderBlendProgram.getValueId("scene"), 0); // GL_TEXTURE0
			gl.glUniform1i(shaderBlendProgram.getValueId("bluredScene"), 1); // GL_TEXTURE1
			gl.glUniform1f(shaderBlendProgram.getValueId("exposure"), exposure);
			gl.glUniform1f(shaderBlendProgram.getValueId("gamma"), gamma);
			gl.glUseProgram(0);
		}
		
		// bind to custom buffers for draw into texture with MRT
		public void begin(GL2 gl) {
			gl.glBindFramebuffer(GL.GL_FRAMEBUFFER, displayFBO.get(0));
			gl.glBindRenderbuffer(GL.GL_RENDERBUFFER, rboBuffer.get(0));
		    gl.glRenderbufferStorage(GL.GL_RENDERBUFFER, GL2.GL_DEPTH_COMPONENT, WIN_W, WIN_H);
		    gl.glFramebufferRenderbuffer(GL.GL_FRAMEBUFFER, GL.GL_DEPTH_ATTACHMENT, GL.GL_RENDERBUFFER, rboBuffer.get(0));
		    gl.glDrawBuffers(2, attachments, 0); // use 2 color components
		    gl.glClear(GL.GL_COLOR_BUFFER_BIT | GL.GL_DEPTH_BUFFER_BIT); // clear it
		    // configuring textures
			gl.glActiveTexture(GL.GL_TEXTURE0);
			for (int i = 0; i < 2; i++) {
				gl.glBindTexture(GL.GL_TEXTURE_2D, colorBuffers.get(i));
				gl.glTexImage2D(GL.GL_TEXTURE_2D, 0, GL.GL_RGB16F, WIN_W, WIN_H, 0, GL.GL_RGB, GL.GL_FLOAT, null);
			}
		}
		
		// bind to default buffers && draw
		public void end(GL2 gl) {
			gl.glBindRenderbuffer(GL.GL_RENDERBUFFER, 0);
			gl.glDrawBuffers(1, attachments, 0); // use 1 color component
			mkBlur(gl);
			gl.glBindFramebuffer(GL.GL_FRAMEBUFFER, 0);
			blend(gl);
			gl.glUseProgram(0);
		}
		
		private void mkBlur(GL2 gl) {
			// configuring textures
			gl.glActiveTexture(GL.GL_TEXTURE0);
			for (int i = 0; i < 2; i++) {
				gl.glBindFramebuffer(GL.GL_FRAMEBUFFER, pingpongFBO.get(i));
				gl.glBindTexture(GL.GL_TEXTURE_2D, pingpongBuffers.get(i));
				gl.glTexImage2D(GL.GL_TEXTURE_2D, 0, GL.GL_RGB16F, WIN_W, WIN_H, 0, GL.GL_RGB, GL.GL_FLOAT, null);
				// Do not need to call glClear, because the texture is completely redrawn
			}
			
			horizontal = true; 
			firstIteration = true;
			gl.glUseProgram(shaderBlurProgram.getProgramId());
			gl.glActiveTexture(GL.GL_TEXTURE0);
			for (int i = 0; i < blurAmount; i++) {
				gl.glBindFramebuffer(GL.GL_FRAMEBUFFER, pingpongFBO.get(bool2int(horizontal)));
				gl.glUniform1i(shaderBlurProgram.getValueId("isHorizontal"), bool2int(horizontal));
				gl.glBindTexture(GL.GL_TEXTURE_2D, firstIteration ? colorBuffers.get(1) : pingpongBuffers.get(bool2int(!horizontal)));
				renderQuad(gl, shaderBlurProgram);
				horizontal = !horizontal;
				if (firstIteration) firstIteration = false;
			}
		}
		
		private void blend(GL2 gl) {
			// Do not need to call glClear, because the texture is completely redrawn
			gl.glUseProgram(shaderBlendProgram.getProgramId());
			gl.glActiveTexture(GL2.GL_TEXTURE0);
			gl.glBindTexture(GL.GL_TEXTURE_2D, colorBuffers.get(0));
			gl.glActiveTexture(GL2.GL_TEXTURE1);
			gl.glBindTexture(GL.GL_TEXTURE_2D, pingpongBuffers.get(bool2int(!horizontal)));
			renderQuad(gl, shaderBlendProgram);
		}
		
		private void applyDefaultTextureParams(GL2 gl) {
			gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MIN_FILTER, GL.GL_LINEAR);
			gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MAG_FILTER, GL.GL_LINEAR);
			gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_WRAP_S, GL.GL_CLAMP_TO_EDGE);
			gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_WRAP_T, GL.GL_CLAMP_TO_EDGE);
		}
		
		private int bool2int(boolean bool) {
			return bool ? 1 : 0;
		}
		
		private void renderQuad(GL2 gl, ShaderProgram shaderProgram) {
			VertexAttribTools.enable(gl, shaderProgram.getValueId("inPos"), shaderProgram.getValueId("inTexCoords"));
			VertexAttribTools.pointer(gl, shaderProgram.getValueId("inPos"), 3, vertexBuffer.rewind());
			VertexAttribTools.pointer(gl, shaderProgram.getValueId("inTexCoords"), 2, texCoordsBuffer.rewind());
			
			gl.glDrawArrays(GL.GL_TRIANGLE_STRIP, 0, 4);
			
			VertexAttribTools.disable(gl, shaderProgram.getValueId("inPos"), shaderProgram.getValueId("inTexCoords"));
		}
	}
}