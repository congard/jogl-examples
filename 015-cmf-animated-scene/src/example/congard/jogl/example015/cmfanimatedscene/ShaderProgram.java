package example.congard.jogl.example015.cmfanimatedscene;

import java.io.File;
import java.util.HashMap;

import com.jogamp.opengl.GL2;
import com.jogamp.opengl.GL3;

/**
 * Manages the shader program.
 * 
 * @author congard
 */
public class ShaderProgram {
	public int programId;
	public int vertexShaderId;
	public int fragmentShaderId;
	public int geometryShaderId;
	private boolean initialized = false;
	public HashMap<String, Integer> valuesIds = new HashMap<String, Integer>();
	
	/**
	 * Initializes the shader program.
	 * 
	 * @param gl context.
	 * @param vertexShader file.
	 * @param fragmentShader file.
	 * @return true if initialization was successful, false otherwise.
	 */
	public boolean init(GL2 gl, File vertexShader, File fragmentShader) {
		return init(gl, vertexShader, fragmentShader, null);
	}
	
	/**
	 * Initializes the shader program. 
	 * 
	 * @param gl
	 * @param vertexShader
	 * @param fragmentShader
	 * @param geometryShader
	 * @return true if initialization was successful, false otherwise.
	 */
	public boolean init(GL2 gl, File vertexShader, File fragmentShader, File geometryShader) {
		if (initialized) {
			throw new IllegalStateException(
					"Unable to initialize the shader program! (it was already initialized)");
		}

		try {
			String vertexShaderCode = ShaderUtils.loadResource(vertexShader.getPath());
			String fragmentShaderCode = ShaderUtils.loadResource(fragmentShader.getPath());
			String geometryShaderCode = null;
			if (geometryShader != null) geometryShaderCode = ShaderUtils.loadResource(geometryShader.getPath());

			programId = gl.glCreateProgram();
			vertexShaderId = ShaderUtils.createShader(gl, programId, vertexShaderCode, GL2.GL_VERTEX_SHADER);
			fragmentShaderId = ShaderUtils.createShader(gl, programId, fragmentShaderCode, GL2.GL_FRAGMENT_SHADER);
			if (geometryShader != null) geometryShaderId = ShaderUtils.createShader(gl, programId, geometryShaderCode, GL3.GL_GEOMETRY_SHADER);
			
			ShaderUtils.link(gl, programId);

			initialized = true;
		} catch (Exception e) {
			e.printStackTrace();
		}

		return initialized;

	}
	
	/**
	 * 
	 * @param name
	 * @return value id, if it was loaded, 0 otherwise
	 */
	public int getValueId(String name) {
		return valuesIds.containsKey(name) ? valuesIds.get(name) : 0;
	}
	
	/**
	 * Load values ids
	 * @param gl
	 * @param values array of GLSLValue
	 */
	public void loadValuesIds(GL2 gl, GLSLValue...values) {
		for (GLSLValue v : values) {
			if (v.type == GLSLValue.ATTRIB) valuesIds.put(v.name, gl.glGetAttribLocation(programId, v.name));
			else if (v.type == GLSLValue.UNIFORM) valuesIds.put(v.name, gl.glGetUniformLocation(programId, v.name));
		}
	}
	
	/**
	 * Destroys the shader program.
	 * 
	 * @param gl2 context.
	 */
	public void dispose(GL2 gl2) {
		initialized = false;
		gl2.glDetachShader(programId, vertexShaderId);
		gl2.glDetachShader(programId, fragmentShaderId);
		gl2.glDeleteProgram(programId);
	}

	/**
	 * @return shader program id.
	 */
	public int getProgramId() {
		if (!initialized) {
			throw new IllegalStateException(
					"Unable to get the program id! The shader program was not initialized!");
		}
		return programId;
	}

	public boolean isInitialized() {
		return initialized;
	}
	
	/**
	 * 
	 * @author congard
	 *
	 */
	public static class GLSLValue {
		// constants
		public static final int ATTRIB = 0, UNIFORM = 1;
		
		public String name;
		public int type;
		
		public GLSLValue(String name, int type) {
			this.name = name;
			this.type = type;
		}
	}
}
