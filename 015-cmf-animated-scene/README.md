# Example 015: CMF Animated Scene
<br><b>Changelog:</b>
1. Updated `libcm 1.2 => 1.3`
1. `bias` set to `0.25`
1. OBJ => CMF models
1. `Lamp.java`: `mLightPosInWorldSpace vec4 => vec3`
1. `Lamps.java`: modificators changed
1. Removed sections of the commented code
1. Compressed angel's textures: 4k => 2k
1. Improving style
1. Added comments in the necessary places
1. Lighting correction (specular, [more information about this correction](https://gitlab.com/congard/jogl-examples/tree/master/README.md))
1. Light source object added: Japanese lamp
1. Added animation to light source object
1. Added timer (shaders, models)
1. `gamma 1.1 > 1.0`
1. `blurAmount 10 => 4`
1. Kernel size 9 (5) => 15 (8)
1. `fragment_blur_shader.glsl`: kernel size changed (`#define KERNEL_SIZE 8`, `5 => kernel.length()`)

![alt scr_0](scr_0.png)

![alt scr_1](scr_1.png)

![alt scr_2](scr_2.png)

![alt scr_3](scr_3.png)

![alt scr_4](scr_4.png)
