package example.congard.jogl.example014.pointlighting;

import java.nio.IntBuffer;

import com.jogamp.opengl.GL;
import com.jogamp.opengl.GL2;
import com.jogamp.opengl.GL3;

import free.lib.congard.ml.geometry.Geometry;
import free.lib.congard.ml.graphics.GMatrix;
import free.lib.congard.ml.graphics.Vec3;
import free.lib.congard.ml.graphics.Vec4;

/**
 * 
 * @author congard
 *
 */
public class Lamp {
	public static final float FAR_PLANE = 32;
	
	// real coordinates (in world)
	public float[] mLightPosInWorldSpace = new float[] {0.0f, 0.0f, 0.0f, 1.0f};
	// light color
	public float[] mLightColor = new float[3];
	// ambient strength param; recommended: 0.01
	public float ambientStrength;
	// diffuse strength param; recommended: 1
	public float diffuseStrength;
	// specular strength param; recommended: 0.1
	public float specularStrength;
	// constant term; usually kept at 1.0
	public float kc;
	// linear term
	public float kl;
	// quadratic term
	public float kq;
	// shininess; recommended: 32
	public int shininess;
	// shadow map, 1024 - default resolution
	public ShadowMapping sm = new ShadowMapping(1024, 1024);
	
	// mathods for ShadowMapping >>
	public Lamp setShadowMapResolution(int width, int height) {
		sm.width = width;
		sm.height = height;
		return this;
	}
	
	public Lamp initShadowMapping(GL3 gl) {
		sm.createDepthMap(gl);
		sm.createDepthMapFBO(gl);
		sm.updateMatrices();
		sm.init(gl);
		return this;
	}
	// <<
	
	// methods for attenuation >>
	public Lamp setKc(float kc) {
		this.kc = kc;
		return this;
	}
	
	public Lamp setKl(float kl) {
		this.kl = kl;
		return this;
	}
	
	public Lamp setKq(float kq) {
		this.kq = kq;
		return this;
	}
	
	public Lamp setAttenuationTerms(float kc, float kl, float kq) {
		this.kc = kc;
		this.kl = kl;
		this.kq = kq;
		return this;
	}
	// <<
	
	public void setX(float x) {
		mLightPosInWorldSpace[0] = x;
	}
	
	public void setY(float y) {
		mLightPosInWorldSpace[1] = y;
	}
	
	public void setZ(float z) {
		mLightPosInWorldSpace[2] = z;
	}
	
	public Lamp setWorldXYZ(float[] xyz) {
		mLightPosInWorldSpace = Vec4.createVec4(xyz);
		return this;
	}
	
	// This 32 value is the shininess value of the highlight.
	// The higher the shininess value of an object, 
	// the more it properly reflects the light instead of scattering it all around and
	// thus the smaller the highlight becomes
	public Lamp setShininess(int shininess) {
		this.shininess = shininess;
		return this;
	}
	
	public Lamp setColor(float r, float g, float b) {
		mLightColor = Vec3.createVec3(r, g, b);
		return this;
	}
	
	public Lamp setAmbientStrength(float ambientStrength) {
		this.ambientStrength = ambientStrength;
		return this;
	}
	
	public Lamp setDiffuseStrength(float diffuseStrength) {
		this.diffuseStrength = diffuseStrength;
		return this;
	}
	
	public Lamp setSpecularStrength(float specularStrength) {
		this.specularStrength = specularStrength;
		return this;
	}
	
	// lamp world position
	public float getX() {
		return mLightPosInWorldSpace[0];
	}
	
	public float getY() {
		return mLightPosInWorldSpace[1];
	}
	
	public float getZ() {
		return mLightPosInWorldSpace[2];
	}
	
	// lamp light color
	public float getLightR() {
		return mLightColor[0];
	}
	
	public float getLightG() {
		return mLightColor[1];
	}
	
	public float getLightB() {
		return mLightColor[2];
	}
	
	/**
	 * 
	 * @author congard
	 *
	 */
	public class ShadowMapping {
		public IntBuffer depthMapFBO, depthCubemap;
		public int width, height;
		public float[] lightProjection = new float[16];
		public float[][] lightViews = new float[6][16];
		public float[][] lightSpaceMatrices = new float[6][16];
		public int[] shadowMatricesLocations = new int[6];
		
		public ShadowMapping(int width, int height) {
			this.width = width;
			this.height = height;
		}
		
		public void initShadowMatrices(GL2 gl, int programId, String array) {
			for (int i = 0; i < 6; i++) shadowMatricesLocations[i] = gl.glGetUniformLocation(programId, array + "[" + i + "]");
		}
		
		public void setLightPos(GL2 gl, int lightPosLocation) {
			gl.glUniform3f(lightPosLocation, getX(), getY(), getZ());
		}
		
		public void setShadowMatrices(GL2 gl) {
			for (int i = 0; i < 6; i++) gl.glUniformMatrix4fv(shadowMatricesLocations[i], 1, false, lightSpaceMatrices[i], 0);
		}
		
		public void updateMatrices() {
			GMatrix.perspectiveM(lightProjection, Geometry.toRadiansf(90), (float)width/(float)height, 1, FAR_PLANE);
			GMatrix.setLookAtM(lightViews[0], 0, getX(), getY(), getZ(), getX() + 1, getY(), getZ(), 0, -1, 0);
			GMatrix.setLookAtM(lightViews[1], 0, getX(), getY(), getZ(), getX() - 1, getY(), getZ(), 0, -1, 0);
			GMatrix.setLookAtM(lightViews[2], 0, getX(), getY(), getZ(), getX(), getY() + 1, getZ(), 0, 0, 1);
			GMatrix.setLookAtM(lightViews[3], 0, getX(), getY(), getZ(), getX(), getY() - 1, getZ(), 0, 0, -1);
			GMatrix.setLookAtM(lightViews[4], 0, getX(), getY(), getZ(), getX(), getY(), getZ() + 1, 0, -1, 0);
			GMatrix.setLookAtM(lightViews[5], 0, getX(), getY(), getZ(), getX(), getY(), getZ() - 1, 0, -1, 0);
			for (int i = 0; i < 6; i++) GMatrix.multiplyMM(lightSpaceMatrices[i], lightProjection, lightViews[i]);
		}
		
		public void createDepthMapFBO(GL3 gl) {
			depthMapFBO = IntBuffer.allocate(1);
			gl.glGenFramebuffers(1, depthMapFBO);
		}
		
		public void createDepthMap(GL3 gl) {
			depthCubemap = IntBuffer.allocate(1);
			gl.glGenTextures(1, depthCubemap);
		}
		
		public void init(GL3 gl) {
			gl.glBindTexture(GL.GL_TEXTURE_CUBE_MAP, depthCubemap.get(0));
			for (int i = 0; i < 6; i++) gl.glTexImage2D(GL.GL_TEXTURE_CUBE_MAP_POSITIVE_X + i, 0, GL2.GL_DEPTH_COMPONENT, width, height, 0, GL2.GL_DEPTH_COMPONENT, GL.GL_FLOAT, null);
			gl.glTexParameteri(GL.GL_TEXTURE_CUBE_MAP, GL.GL_TEXTURE_MAG_FILTER, GL.GL_NEAREST);
			gl.glTexParameteri(GL.GL_TEXTURE_CUBE_MAP, GL.GL_TEXTURE_MIN_FILTER, GL.GL_NEAREST);
			gl.glTexParameteri(GL.GL_TEXTURE_CUBE_MAP, GL.GL_TEXTURE_WRAP_S, GL.GL_CLAMP_TO_EDGE);
			gl.glTexParameteri(GL.GL_TEXTURE_CUBE_MAP, GL.GL_TEXTURE_WRAP_T, GL.GL_CLAMP_TO_EDGE);
			gl.glTexParameteri(GL.GL_TEXTURE_CUBE_MAP, GL2.GL_TEXTURE_WRAP_R, GL.GL_CLAMP_TO_EDGE);
			gl.glBindFramebuffer(GL.GL_FRAMEBUFFER, depthMapFBO.get(0));
			gl.glFramebufferTexture(GL.GL_FRAMEBUFFER, GL.GL_DEPTH_ATTACHMENT, depthCubemap.get(0), 0);
			gl.glDrawBuffer(GL.GL_NONE);
			gl.glReadBuffer(GL.GL_NONE);
			gl.glBindFramebuffer(GL.GL_FRAMEBUFFER, 0);
			gl.glBindTexture(GL.GL_TEXTURE_CUBE_MAP, 0);
		}
		
		public void begin(GL2 gl) {
			gl.glBindFramebuffer(GL.GL_FRAMEBUFFER, depthMapFBO.get(0));
			gl.glActiveTexture(GL.GL_TEXTURE0);
			gl.glBindTexture(GL.GL_TEXTURE_CUBE_MAP, depthCubemap.get(0));
			gl.glViewport(0, 0, width, height);
		}
		
		public void end(GL2 gl) {
			gl.glBindFramebuffer(GL.GL_FRAMEBUFFER, 0);
			gl.glBindTexture(GL.GL_TEXTURE_CUBE_MAP, 0);
		}
	}
}
